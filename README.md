# OpenML dataset: a5a

https://www.openml.org/d/1426

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Ronny Kohavi","Barry Becker  
libSVM","AAD group  
**Source**: [original](http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary.html) - Date unknown  
**Please cite**: http://archive.ics.uci.edu/ml/datasets/Adult  

#Dataset from the LIBSVM data repository.

Preprocessing: The original Adult data set has 14 features, among which six are continuous and eight are categorical. In this data set, continuous features are discretized into quantiles, and each quantile is represented by a binary feature. Also, a categorical feature with m categories is converted to m binary features. Details on how each feature is converted can be found in the beginning of each file from this page. 
http://research.microsoft.com/en-us/um/people/jplatt/adult.zip

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1426) of an [OpenML dataset](https://www.openml.org/d/1426). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1426/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1426/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1426/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

